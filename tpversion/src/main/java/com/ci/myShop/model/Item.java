/**
 * 
 */
package com.ci.myShop.model;

import java.util.List;

/**
 * @author leoma
 *
 */
public class Item {
	String name="";
	int id;
	float price;
	int nbrElt;
	
	
	
	public Item(String name, int id, float price, int nbrElt) {
		super();
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
	}
	
	public String getName() {
		return name;
	}
	private void setName(String name) {
		this.name = name;
	}
	private int getId() {
		return id;
	}
	private void setId(int id) {
		this.id = id;
	}
	public float getPrice() {
		return price;
	}
	private void setPrice(float price) {
		this.price = price;
	}
	public int getNbrElt() {
		return nbrElt;
	}
	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
	public String display() {
		
		return "";
	}
	
	
	
	public class Consumable extends Item {

        int quantity;
        public Consumable(String name, int id, float price, int nbrElt, int quantity) {
			super(name, id, price, nbrElt);
			this.quantity=quantity;
			// TODO Auto-generated constructor stub
		}
		private int getQuantity() {
			return quantity;
		}
		private void setQuantity(int quantity) {
			this.quantity = quantity;
		}
      
		
       
        }
	
	class Paper extends Consumable {

        String quality;     
        float weigh ; 
        
        
        
		public Paper(String name, int id, float price, int nbrElt, int quantity, String quality, float weigh) {
			super(name, id, price, nbrElt, quantity);
			this.quality=quality;
			this.quantity=quantity;
			// TODO Auto-generated constructor stub
		}
		
		
		String getQuality() {
			return quality;
		}
		private void setQuality(String quality) {
			this.quality = quality;
		}
		private float getWeigh() {
			return weigh;
		}
		private void setWeigh(float weigh) {
			this.weigh = weigh;
		}

}



	public class Pen extends Consumable {

       String color ; 
       int durability ;
       public Pen(String name, int id, float price, int nbrElt, int quantity, String color, int durability) {
			super(name, id, price, nbrElt, quantity);
			this.color=color;
			this.durability=durability; 
			// TODO Auto-generated constructor stub
		}

		
        

		private String getColor() {
			return color;
		}

		private void setColor(String color) {
			this.color = color;
		}

		private int getDurability() {
			return durability;
		}

		private void setDurability(int durability) {
			this.durability = durability;
		}




}

	public class Book extends Item {
		int nbPage ;
        String author ;
        String publisher ;
        int year ;
        int age ;
		


		

		public Book(String name, int id, float price, int nbrElt,int nbPage, String author,String publisher,int year,int age) {
			super(name, id, price, nbrElt);
			this.nbPage=nbPage;
			this.author=author;
			this.publisher=publisher;
			this.year=year;
			this.age=age;
			
			
			// TODO Auto-generated constructor stub
		}

		private int getNbPage() {
			return nbPage;
		}

		private void setNbPage(int nbPage) {
			this.nbPage = nbPage;
		}

		private String getAuthor() {
			return author;
		}

		private void setAuthor(String author) {
			this.author = author;
		}

		private String getPublisher() {
			return publisher;
		}

		private void setPublisher(String publisher) {
			this.publisher = publisher;
		}

		private int getYear() {
			return year;
		}

		private void setYear(int year) {
			this.year = year;
		}

		private int getAge() {
			return age;
		}

		private void setAge(int age) {
			this.age = age;
		}

		



}



	public class BooktoTouch extends Book{

        String material ;
        int durability ;

		public BooktoTouch(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
				int year, int age, String material, int durability) {
			super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
			this.material=material;
			this.durability=durability;
			
			// TODO Auto-generated constructor stub
		}

		

		private String getMaterial() {
			return material;
		}

		private void setMaterial(String material) {
			this.material = material;
		}

		private int getDurability() {
			return durability;
		}

		private void setDurability(int durability) {
			this.durability = durability;
		}


}

	public class MusicalBook extends Book {

        List<String>listOfSound ;
        int lifetime ;
        int nbrBattery ;
        
        public MusicalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
				int year, int age,List<String>listOfSound,int lifetime,int nbrBattery) {
        	
			super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
			this.listOfSound=listOfSound;
			this.lifetime=lifetime;
			this.nbrBattery=nbrBattery;
			
			// TODO Auto-generated constructor stub
		}
		
        
        
        
		List<String> getListOfSound() {
			return listOfSound;
		}
		private void setListOfSound(List<String> listOfSound) {
			this.listOfSound = listOfSound;
		}
		private int getLifetime() {
			return lifetime;
		}
		private void setLifetime(int lifetime) {
			this.lifetime = lifetime;
		}
		private int getNbrBattery() {
			return nbrBattery;
		}
		private void setNbrBattery(int nbrBattery) {
			this.nbrBattery = nbrBattery;
		}



}

	public class PuzzleBook extends Book {

    int nbrPieces ;

	public PuzzleBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
				int year, int age, int nbrPieces) {
			super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
			this.nbrPieces=nbrPieces;
			// TODO Auto-generated constructor stub
		}

	

	private int getNbrPieces() {
		return nbrPieces;
	}

	private void setNbrPieces(int nbrPieces) {
		this.nbrPieces = nbrPieces;
	}
	


}

	public class OriginalBook extends Book {

        boolean isNumeric=true;

		public OriginalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
				int year, int age, boolean isNumeric) {
			super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
			this.isNumeric=isNumeric;
			// TODO Auto-generated constructor stub
		}

		

		private boolean isNumeric() {
			return isNumeric;
		}

		private void setNumeric(boolean isNumeric) {
			this.isNumeric = isNumeric;
		}


}

}
