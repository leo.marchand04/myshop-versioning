/**
 * 
 */
package com.ci.myShop.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ci.myShop.model.Item;
import com.ci.myShop.model.Item.Book;

/**
 * @author leoma
 *
 */
public class Shop {
	
	static Item item1;
	static Item item2;
	
	public Shop(Storage storage, float cash) {
		super();
		this.storage = storage;
		this.cash = cash;
	}

	Storage storage=new Storage();
	float cash=(float) 0.0;
	
	public Item sell(String name) {
		Item itemTmp=storage.getItem (name);
		if(itemTmp!=null) {
			
			
			int nbrElmtTmp=itemTmp.getNbrElt();
			nbrElmtTmp=nbrElmtTmp-1;
			itemTmp.setNbrElt(nbrElmtTmp);
			
			cash=cash+itemTmp.getPrice();			
			return itemTmp;
			
			
			
		}
		else return null;
	}
	private void Launch () {
		List<Item>listeItem;
		listeItem = new ArrayList<Item>();
		listeItem.add(item1);
		listeItem.add(item2);
		
		Storage storage=new Storage();
		Shop shop= new Shop(storage,(float)0.0);
		
		storage.addItemList(listeItem);
		shop.sell(item1.getName());
		System.out.println(shop.getCash());
	}

	private Storage getStorage() {
		return storage;
	}

	private void setStorage(Storage storage) {
		this.storage = storage;
	}

	public float getCash() {
		return cash;
	}

	private void setCash(float cash) {
		this.cash = cash;
	}
	public boolean buy(Item item) {
		float price ;
		boolean itemAchete; 
		price= item.getPrice();
		if (cash>=price) {
			itemAchete=true;
			cash=cash-price;
			return itemAchete;
		}
		else itemAchete=false;
			return itemAchete;
	}


	
	public boolean isItemAvailable(String name) {
		boolean itemAvailable;
		Item item=storage.getItem (name);
		if(item!=null) {
			if (item.getNbrElt()>0) {
				itemAvailable=true;
				System.out.println("L'item "+name+" est disponible");
				return itemAvailable;
				
				
			}else
				itemAvailable=false;
			System.out.println("L'item "+name+" n'est pas disponible");
			return itemAvailable=false;
			
		}
		
		
		else System.out.println("L'item "+name+" ,n'éxiste pas");
		return itemAvailable=false;

		
		
		
		
		
	}
	
	 public int getAgeForBook(String name) {
		 Item itemTmp=Item.getItem(name);
		 Book book=itemTmp;
		 int age=book.getAge();
		 return age;
	 }
}
		

		
	

